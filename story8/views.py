from django.shortcuts import render

#Story 8
def index(request):
    context = {
        "page" : "storys8"
    }
    return render(request, "story8/index.html", context)